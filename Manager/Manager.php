<?php

include_once '../Model/Book.php';
include_once '../Model/DVD.php';
include_once '../Model/Furniture.php';
include_once 'db_info.php';
/*
 * Manager class handles all interactions with database.
 */
class Manager
{
    /*
     * creating only one instance of manager class and accessing it via static getInstance function.
     */
    private static $instance = null;
    public static function getInstance(){
        if (self::$instance == null){
            self::$instance = new Manager();
        }
        return self::$instance;
    }



    private $conn;

    /*
     * creating connection with the database
     */
    private function __construct() {
        $servername = db_info::$MYSQL_SERVER;
        $username = db_info::$MYSQL_USERNAME;
        $password = db_info::$MYSQL_PASSWORD;
        $data_base = db_info::$MYSQL_DATABASE_NAME;
        $this->conn = new mysqli($servername, $username, $password, $data_base);
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
    }

    /*
     * closing connection with the database
     */
    function __destruct() {
        $this->conn->close();
    }

    /*
     * returns array of all books from the BOOK table
     */
    private function getBooks(){
        $res = array();
        $sql = 'select * from BOOK';
        $result = $this->conn->query($sql);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $curBook = new Book($row["SKU"], $row["name"], $row["price"], $row["weight"]);
                array_push($res, $curBook);
            }
        }
        return $res;
    }

    /*
     * returns array of all dvds from the DVD table
     */
    private function getDVDs(){
        $res = array();
        $sql = 'select * from DVD';
        $result = $this->conn->query($sql);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $curDVD = new DVD($row["SKU"], $row["name"], $row["price"], $row["size"]);
                array_push($res, $curDVD);
            }
        }
        return $res;
    }

    /*
     * returns array of all furniture from the FURNITURE table
     */
    private function getFurniture(){
        $res = array();
        $sql = 'select * from FURNITURE';
        $result = $this->conn->query($sql);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $curFurniture = new Furniture($row["SKU"], $row["name"], $row["price"], $row["h"], $row["w"], $row["l"]);
                array_push($res, $curFurniture);
            }
        }
        return $res;
    }

    /*
     * returns whole data from the database, including books, dvds and furniture
     */
    public function getAllData(){
        $books = $this->getBooks();
        $dvds = $this->getDVDs();
        $furniture = $this->getFurniture();
        return array_merge($books, $dvds, $furniture);
    }


    /*
     * checks if SKU already exists in the database.
     * if SKU already exists prints 'SKU already exists, try different one'
     * if SKU doesn't exist in the database prints '<div id="sku-success">SKU can be used</div>'
     * messages are displayed in the 'Add.php' page via ajax.
     */
    public function validateSKU($SKU){
        $tables = array("BOOK", "DVD", "FURNITURE");
        foreach ($tables as $curTable) {
            $sql = 'SELECT COUNT(*) res FROM '. $curTable .' WHERE SKU = "' . $SKU . '";';
            $result = $this->conn->query($sql);
            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    if($row["res"] > 0){
                        echo 'SKU already exists, try different one';
                        return;
                    }
                }
            }
        }
        echo '<div id="sku-success">SKU can be used</div>';
    }

    /*
     * executes given query
     */
    public function executeQuery($query){
        $this->conn->query($query);
    }
}

?>