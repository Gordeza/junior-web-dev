<?php

/*
 * Abstract class Product, which will be extended by Book, DVD and Furniture classes.
 * All the children classes will have methods: getSKU(), getName(), getPrice() with same implementation.
 * All the children classes have same getDetails() method with implementation based on the class.
 */

abstract class Product
{
    public static function format($str){
        return "'" . $str . "'";
    }

    protected $SKU, $name, $price, $table;
    public function getSKU()
    {
        return $this->SKU;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price . '$';
    }

    public function getTable(){
        return $this->table;
    }

    public function deleteProduct(){
        $sql = 'DELETE FROM ' . $this->table . ' WHERE SKU="' . $this->SKU . '";';
        $mng = Manager::getInstance();
        $mng->executeQuery($sql);
    }

    abstract public function getDetails();
    abstract public function addProduct();

}

?>