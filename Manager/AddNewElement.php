<?php
include_once '../Manager/Manager.php';

/*
 * storing names of classes in array as key (value of selected type) and value (name of class) pairs.
 * certain object will be created based on the value of Type Switcher.
 */
$classes = array("book-select"=>"Book","dvd-select"=>"DVD","furniture-select"=>"Furniture");
$select = $_POST["select"];
$newElem = new $classes[$select]();
$newElem->addProduct();
header('Location: /../ProductPages/List.php');
?>