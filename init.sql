USE scandiweb;

CREATE TABLE BOOK(
    SKU varchar(25) unique,
    name varchar(20),
    price int,
    weight int
);

CREATE TABLE DVD(
    SKU varchar(25) unique,
    name varchar(20),
    price int,
    size int
);

CREATE TABLE FURNITURE(
    SKU varchar(25) unique,
    name varchar(20),
    price int,
    h int,
    w int,
    l int
);

insert into BOOK (SKU, name, price, weight)
values
('SKU-book1', 'book1', 20, 4),
('SKU-book2', 'book2', 22, 4),
('SKU-book3', 'book3', 24, 4),
('SKU-book4', 'book1', 123, 4),
('SKU-book5', 'book2', 25, 4),
('SKU-book6', 'book3', 229, 4),
('SKU-book7', 'book1', 20, 4),
('SKU-book8', 'book2', 20, 4),
('SKU-book9', 'book3', 10, 4),
('SKU-book10', 'book3', 25, 4),
('SKU-book11', 'book3', 30, 4)
;

insert into DVD (SKU, name, price, size)
values
('SKU-dvd1', 'dvd1', 50, 399),
('SKU-dvd2', 'dvd2', 20, 460);

insert into FURNITURE (SKU, name, price, h, w, l)
values
('SKU-fur1', 'fur1', 100, 1, 2, 3),
('SKU-fur2', 'fur2', 200, 4, 5, 1);