<?php

include_once 'Product.php';

class Furniture extends Product
{

    //private variables defined for furniture class
    private $h, $w, $l;

    /*

        constructor that may take multiple number of arguments.
        private __construct1 is the main function that deals constructing object. __construct passes arguments to __construct1.
        if 6 arguments are given, all the arguments are used for constructing. used when getting data from DB.
        if 1 argument is given, only sku is initialized. used for deleting data from DB.
        if no arguments are given, initializing object from POST data. used when adding new product to DB.

    */
    public function __construct()
    {
        $argv = func_get_args();

        switch(func_num_args()) {
            case 6:
                self::__construct1($argv[0], $argv[1], $argv[2], $argv[3], $argv[4], $argv[5]);
                break;
            case 1:
                self::__construct1($argv[0], "", "", "", "", "");
                break;
            case 0:
                self::__construct1($_POST["sku"], $_POST["name"], $_POST["price"], $_POST["height"], $_POST["width"], $_POST["length"]);
                break;
            default:
                die('invalid number of arguments');
        }
    }


    //constructor based on furniture class
    private function __construct1($SKU, $name, $price, $h, $w, $l)
    {
        $this->SKU = $SKU;
        $this->name = $name;
        $this->price = $price;
        $this->h = $h;
        $this->w = $w;
        $this->l = $l;
        $this->table = "FURNITURE";
    }

    //implementing abstract method based on the furniture class
    public function getDetails()
    {
        return 'Dimension: ' . $this->h . 'x' . $this->w . 'x' . $this->l;
    }

    public function addProduct()
    {
        $params = '('. Product::format($this->SKU) . ',' . Product::format($this->name) . ',' . $this->price . ',' . $this->h . ',' . $this->w . ',' . $this->l . ');';
        $sql = 'insert into FURNITURE (SKU, name, price, h, w, l)
            values ' . $params;
        $mng = Manager::getInstance();
        $mng->executeQuery($sql);
    }



}

?>