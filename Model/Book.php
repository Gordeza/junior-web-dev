<?php

include_once 'Product.php';
include_once '../Manager/Manager.php';

class Book extends Product
{
    //private variables defined for book class
    private $weight;

    /*

        constructor that may take multiple number of arguments.
        private __construct1 is the main function that deals constructing object. __construct passes arguments to __construct1.
        if 4 arguments are given, all the arguments are used for constructing. used when getting data from DB.
        if 1 argument is given, only sku is initialized. used for deleting data from DB.
        if no arguments are given, initializing object from POST data. used when adding new product to DB.

    */
    public function __construct()
    {
        $argv = func_get_args();

        switch(func_num_args()) {
            case 4:
                self::__construct1($argv[0], $argv[1], $argv[2], $argv[3]);
                break;
            case 1:
                self::__construct1($argv[0], "", "", "");
                break;
            case 0:
                self::__construct1($_POST["sku"], $_POST["name"], $_POST["price"], $_POST["weight"]);
                break;
            default:
                die('invalid number of arguments');
        }
    }

    //constructor based on book class
    private function __construct1($SKU, $name, $price, $weight)
    {
        $this->SKU = $SKU;
        $this->name = $name;
        $this->price = $price;
        $this->weight = $weight;
        $this->table = "BOOK";
    }


    //implementing abstract method based on the book class
    public function getDetails()
    {
        return 'Weights: ' . $this->weight . 'KG';
    }

    public function addProduct()
    {
        $params = '('. Product::format($this->SKU) . ',' . Product::format($this->name) . ',' . $this->price . ',' . $this->weight . ');';
        $sql = 'insert into BOOK (SKU, name, price, weight)
            values ' . $params;
        $mng = Manager::getInstance();
        $mng->executeQuery($sql);
    }

}

?>