<?php
include_once '../Manager/Manager.php';

$mng = Manager::getInstance();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Product List</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../CSS/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
<?php

/*
 * if mass delete button was pressed collecting checked data and deleting it
 */
if(isset($_POST['submit'])){
    if(!empty($_POST['checkArr'])){
        foreach($_POST['checkArr'] as $checked){
            $posOfTok = strpos($checked, '?');
            $val = substr($checked, 0, $posOfTok);
            $class = substr($checked, $posOfTok + 1);
            $toDel = new $class($val);
            $toDel->deleteProduct();
        }
    }
}
?>




<form action="List.php" method="post">

    <nav class="navbar navbar-expand-lg navbar-light ">
        <a class="navbar-brand" href="#">Product List</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
            </ul>
            <div class="form-inline my-2 my-lg-0">
                <a href="Add.php" class="btn btn-dark my-2 my-sm-0">ADD</a>
                <ul></ul>
                <input type="submit" name="submit" value="MASS DELETE" class="btn btn-dark my-2 my-sm-0"/>
                <ul></ul>
            </div>
        </div>
    </nav>
    <hr style="height:2px;border-width:0;color:gray;background-color:gray">
    <br>




    <div class="container">
        <?php
        /*
         * getting all data from database and displaying
         */
        $i = 0;
        $arr = $mng->getAllData();
        foreach ($arr as $cur) {
            if($i % 4 == 0){
                echo '<div class="row">';
            }
        ?>
            <div class="col-md-5 col-lg-3">
                <div class="list-elem">
                    <div class="form-check">
                        <input type="checkbox" name="checkArr[]" value="<?php echo $cur->getSKU() . '?' . get_class($cur)?>">
                    </div>
                    <div class="text-center">
                        <p><?php echo $cur->getSKU()?></p>
                        <p><?php echo $cur->getName()?></p>
                        <p><?php echo $cur->getPrice()?></p>
                        <p><?php echo $cur->getDetails()?></p>
                    </div>
                </div>
            </div>
        <?php
            if($i % 4 == 3){
                echo '</div><br>';
            }
            $i++;
        }
        if($i % 4 != 0){
            echo '</div><br>';
        }
        ?>

    </div>
</form>



<hr style="height:2px;border-width:0;color:gray;background-color:gray">
<div class="text-center">
    <p>
        Scandiweb Test Assignment
    </p>
</div>


</body>

</html>