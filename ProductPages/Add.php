<!DOCTYPE html>
<html lang="en">
<head>
    <title>Product Add</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../CSS/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="../JS/add.js"></script>

</head>

<body>
<form id="myForm" action="../Manager/AddNewElement.php" method="post">
    <nav class="navbar navbar-expand-lg navbar-light ">
        <a class="navbar-brand" href="#">Product Add</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
            </ul>
            <div class="form-inline my-2 my-lg-0">
                <input type="submit" name="submit" value="Save" class="btn btn-dark my-2 my-sm-0"/>
                <ul></ul>
                <a href="List.php" class="btn btn-dark my-2 my-sm-0">Cancel</a>
                <ul></ul>
            </div>
        </div>
    </nav>
    <hr style="height:2px;border-width:0;color:gray;background-color:gray">
    <br>

<!--    form group.-->
<!--    each parameter has its own input and error sections.-->
<!--    error sections are empty in the beginning and are filled via javascript functions if an error occurs.-->
    <div class="container">

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">SKU</label>
                <div class="col-sm-10">
                    <input type="text" name="sku" class="form-control" id="sku-place" placeholder="SKU">
                    <div class="error-text" id="sku-error">
                    </div>
                </div>
            </div>


            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" id="name-place" placeholder="Name">
                    <div class="error-text" id="name-error">
                    </div>
                </div>
            </div>


            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Price($)</label>
                <div class="col-sm-10">
                    <input type="text" name="price" class="form-control" id="price-place" placeholder="Price">
                    <div class="error-text" id="price-error">
                    </div>
                </div>
            </div>


            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Type Switcher</label>
                <div class="col-sm-10">
                    <select name="select" class="form-control" id="select-type">
                        <option selected value="base">Type Switcher</option>
                        <option value="dvd-select">DVD</option>
                        <option value="book-select">Book</option>
                        <option value="furniture-select">Furniture</option>
                    </select>
                    <div class="error-text" id="select-error">

                    </div>
                </div>
            </div>

<!--        everything is hidden and depends on which value is selected in Type Switcher   -->
<!--        when switched, javascript function is called, so selected value's fields are displayed   -->

            <div>
                <div id="book-select" class="dynamic-select invisible">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Weight (KG)</label>
                        <div class="col-sm-10">
                            <input type="text" name="weight" class="form-control" id="weight-place" placeholder="">
                            <div class="error-text" id="weight-error">

                            </div>
                        </div>
                    </div>
                    <p>Please, provide weight</p>
                </div>


                <div id="dvd-select" class="dynamic-select invisible">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Size (MB)</label>
                        <div class="col-sm-10">
                            <input type="text" name="size" class="form-control" id="size-place" placeholder="">
                            <div class="error-text" id="size-error">

                            </div>
                        </div>
                    </div>
                    <p>Please, provide size</p>
                </div>


                <div id="furniture-select" class="dynamic-select invisible">

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Height (CM)</label>
                        <div class="col-sm-10">
                            <input type="text" name="height" class="form-control" id="height-place" placeholder="">
                            <div class="error-text" id="height-error">

                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Width (CM)</label>
                        <div class="col-sm-10">
                            <input type="text" name="width" class="form-control" id="width-place" placeholder="">
                            <div class="error-text" id="width-error">

                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Length (CM)</label>
                        <div class="col-sm-10">
                            <input type="text" name="length" class="form-control" id="length-place" placeholder="">
                            <div class="error-text" id="length-error">

                            </div>
                        </div>
                    </div>
                    <p>Please, provide dimensions</p>
                </div>
            </div>

    </div>
</form>


</body>

</html>