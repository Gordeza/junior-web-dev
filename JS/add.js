$(document).ready(function(){

        /*
        displaying paramaters base on the selected value.
        is triggered when value is changed.
        */
    $("#select-type").change(function() {
        var elems = document.getElementsByClassName("dynamic-select");
        for (var i = 0; i < elems.length; i++) {
            elems[i].classList.add("invisible");
        }
        var elem = document.getElementById(this.value);
        elem.classList.remove("invisible");
    });



        /*
        * using ajax to see if written value of SKU already exists in database, as it must be unique.
        * if value already exists, 'SKU already exists, try different one' error message shows up.
        * if value can be used 'SKU can be used' confirmation message shows up.
        */

    function checkSKU(sku){
        if (sku.length == 0) {
            document.getElementById("sku-error").innerHTML = "";
            return;
        }
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("sku-error").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET", "../ProductPages/validateSKU.php?sku=" + sku, true);
        xmlhttp.send();
    }


    document.getElementById('sku-place').addEventListener("keyup", function(){
        checkSKU(this.value);
    }, false);





        /*
        validation of value.
        checks that value isn't empty.
        if validateNumber argument is true checks that value consists of only numbers.
        if validation fails returns relevant error message, which will be written in error place.
        if validation passes returns empty string.
        */
    function validateField(val, validateNumber){
        if(val == ""){
            return "Please, submit required data";
        }
        if(validateNumber){
            for (var i = 0; i < val.length; i++) {
                if(val.charAt(i) < '0' || val.charAt(i) > '9'){
                    return "Please, provide the data of indicated type";
                }
            }
        }
        return "";
    }


        /*
        before submittig validating all values.
        if validation failes variable canSubmit becomes false and parameters won't be sent.
        each failed parameter will have it's own error message
        */
        const form = document.getElementById("myForm");
        form.addEventListener('submit', (e) => {
        var canSubmit = true;

        /*
            validation of sku.
            field mustn't be empty.
            value mustn't be in database, it must be unique.
         */
        var curField = document.getElementById("sku-place");
        var curErrorField = document.getElementById("sku-error");
        if(curField.value == ""){
        curErrorField.innerHTML = "Please, submit required data";
        canSubmit = false;
    }else{
        //checking if got no error message from sku validation
        if(curErrorField.innerHTML === "SKU already exists, try different one"){
        canSubmit = false;
    }
    }


        /*
            validation of name.
            field mustn't be empty
         */
        curField = document.getElementById("name-place");
        curErrorField = document.getElementById("name-error");
        var err = validateField(curField.value, false);
        curErrorField.innerHTML = err;
        if(err !== ""){
        canSubmit = false;
    }

        /*
            validation of price.
            field mustn't be empty.
            field must contain only numbers
         */
        curField = document.getElementById("price-place");
        curErrorField = document.getElementById("price-error");
        err = validateField(curField.value, true);
        curErrorField.innerHTML = err;
        if(err !== ""){
        canSubmit = false;
    }

        /*
            validation of select.
            something must be selected excluding base parameter 'Type Switcher'
         */
        curField = document.getElementById("select-type");
        curErrorField = document.getElementById("select-error");
        if(curField.value == "base"){
        curErrorField.innerHTML = "Please, submit required data";
        canSubmit = false;
    }else{
        curErrorField.innerHTML = "";
    }

        /*
            validation of weight if book is selected.
            field mustn't be empty.
            field must contain only numbers
         */
        if(curField.value == "book-select"){
        var x = document.getElementById("weight-place");
        curErrorField = document.getElementById("weight-error");
        err = validateField(x.value, true);
        curErrorField.innerHTML = err;
        if(err !== ""){
        canSubmit = false;
    }
    }else{
        document.getElementById("weight-error").innerHTML = "";
    }

        /*
            validation of size if dvd is selected.
            field mustn't be empty.
            field must contain only numbers
         */
        if(curField.value == "dvd-select"){
        var x = document.getElementById("size-place");
        curErrorField = document.getElementById("size-error");
        err = validateField(x.value, true);
        curErrorField.innerHTML = err;
        if(err !== ""){
        canSubmit = false;
    }
    }else{
        document.getElementById("size-error").innerHTML = "";
    }

        /*
            validation of length, width and height if furniture is selected.
            fields mustn't be empty.
            fields must contain only numbers
         */
        if(curField.value == "furniture-select"){
        var x = document.getElementById("height-place");
        curErrorField = document.getElementById("height-error");
        err = validateField(x.value, true);
        curErrorField.innerHTML = err;
        if(err !== ""){
        canSubmit = false;
    }

        x = document.getElementById("width-place");
        curErrorField = document.getElementById("width-error");
        err = validateField(x.value, true);
        curErrorField.innerHTML = err;
        if(err !== ""){
        canSubmit = false;
    }

        x = document.getElementById("length-place");
        curErrorField = document.getElementById("length-error");
        err = validateField(x.value, true);
        curErrorField.innerHTML = err;
        if(err !== ""){
        canSubmit = false;
    }
    }else{
        document.getElementById("length-error").innerHTML = "";
        document.getElementById("height-error").innerHTML = "";
        document.getElementById("width-error").innerHTML = "";
    }



        if(canSubmit == false){
        e.preventDefault()
        }
    })


});